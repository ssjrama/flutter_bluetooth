// For performing some operations asynchronously
import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';

// For using PlatformException
import 'package:flutter/services.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';
import 'package:splashscreen/splashscreen.dart';
import 'package:flutter_bluetooth/home.dart';
import 'package:flutter_bluetooth/setting.dart';
import 'package:flutter_bluetooth/intro.dart';

// void main() => runApp(MyApp());
// void main() => runApp(MyApp());
void main() {
  runApp(new MaterialApp(
    home: new MyApp(),
    initialRoute: '/intro',
    routes: {
      '/intro': (context) => Intro(),
      '/home': (context) => Home(),
      '/setting': (context) => Setting(),
    },
  ));
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return new SplashScreen(
      seconds: 14,
      navigateAfterSeconds: new Intro(),
      title: new Text(
        'Welcome In SplashScreen',
        style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
      ),
      image: new Image.network(
          'https://flutter.io/images/catalog-widget-placeholder.png'),
      backgroundColor: Colors.white,
      loaderColor: Colors.red,
    );
  }
}
