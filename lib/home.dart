import 'package:flutter/material.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('DISDROMETER'),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 200.0),
        child: Column(
          children: [
            Row(
              children: [
                Expanded(
                  child: Text('device_name', textAlign: TextAlign.center),
                ),
              ],
            ),
            SizedBox(
              height: 20.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Column(
                  children: [
                    IconButton(
                      icon: const Icon(Icons.settings),
                      tooltip: 'Setting',
                      onPressed: () {
                        Navigator.pushNamed(context, '/setting');
                      },
                    ),
                    Text('Setting')
                  ],
                ),
                SizedBox(
                  width: 20.0,
                ),
                Column(
                  children: [
                    IconButton(
                      icon: const Icon(Icons.download_sharp),
                      tooltip: 'Download',
                      onPressed: () {},
                    ),
                    Text('Download')
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
